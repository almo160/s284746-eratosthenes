all: eratosthenes

eratosthenes: eratosthenes.cpp
	$(CXX) $(CXXFLAGS) -std=c++11 -pedantic -Werror -Wall -o eratosthenes eratosthenes.cpp

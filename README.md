# Sieve of Eratosthenes / Решето Эратосфена
Possible implementation for Sieve of Eratosthenes in C++
<br/>Пример реализации решета Эратосфена на C++

## Author / Автор
Evgeny Ivanov @almo160<br/>
Евгений Иванов @almo160

## Created on / Создано
March 16, 2023
<br/>16 марта 2023

## Description / Описание
Sieve of Eratosthenes is used for searching prime numbers in given range.<br/>
Решето Эратосфена используется для поиска простых чисел в заданном диапазоне.

## Installation / Установка
No installation needed. To build on Unix-like OS, run <code>make</code>. The executable file will be <code>eratosthenes</code>.<br/>
Установка не требуется. Для сборки на Unix-подобной ОС, выполните <code>make</code>. Исполняемый файл будет называться <code>eratosthenes</code>.

## Usage / Использование
To find all prime numbers in range from 1 to <code>LIMIT</code>, run <code>./eratosthenes LIMIT</code> or simply <code>./eratosthenes</code> and then enter LIMIT. Result will be printed to <code>output.txt</code> file.<br/>
Чтобы найти все простые числа в диапазоне от 1 до <code>LIMIT</code>, выполните <code>./eratosthenes LIMIT</code> или просто <code>./eratosthenes</code> и затем введите LIMIT. Результат будет выведен в файл <code>output.txt</code>.
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdexcept>

namespace s284746 {
	struct LIST_NODE {
		int val;
		LIST_NODE *next;
		LIST_NODE() = delete;
		LIST_NODE(int value, LIST_NODE *next_node) : val(value), next(next_node) {}
		LIST_NODE(int value) : LIST_NODE(value, nullptr) {}
	};
	int sqrt(int x) {
		if (x < 0) throw std::out_of_range("Argument of sqrt must not be negative");
		if (x == 0) return 0;
		uint64_t arg = x;
		uint64_t res = 1;
		while (res * res * 4 <= arg) res *= 2;
		uint64_t factor = res / 2;
		while (factor > 0) {
			uint64_t tmp = res + factor;
			if (tmp * tmp <= arg) res = tmp;
			factor /= 2;
		}
		return res;
	}
	int log2(int x) {
		if (x <= 0) throw std::out_of_range("Argument of log2 must be positive");
		int count = 0;
		while (x > 1) {
			++count;
			x /= 2;
		}
		return count;
	}
	void print_list(const LIST_NODE *list, std::ostream &out) {
		for (const LIST_NODE *ptr = list; ptr; ptr = ptr->next) out << ptr->val << std::endl;
	}
	void remove_list(LIST_NODE *list) {
		if (list) {
			if (list->next) remove_list(list->next);
			delete list;
		}
	}
	LIST_NODE* prime_numbers_dumb(int n) {
		if (n > 1) {
			LIST_NODE *head = new LIST_NODE(2);
			LIST_NODE *tail = head;
			int curr = 3;
			while (curr <= n) {
				tail->next = new LIST_NODE(curr);
				tail = tail->next;
				curr += 2;
			}
			if (n < 9) return head;
			int k = sqrt(n);
			LIST_NODE *active = head;
			while (active->val <= k) {
				LIST_NODE *prev = active;
				LIST_NODE *ptr = prev->next;
				while (ptr) {
					LIST_NODE *next = ptr->next;
					if (ptr->val % active->val) prev = ptr;
					else {
						prev->next = next;
						delete ptr;
					}
					ptr = next;
				}
				active = active->next;
			}
			return head;
		}
		return nullptr;
	}
	LIST_NODE* prime_numbers_as_list(int n) {
		if (n > 100) {
			int k = sqrt(n);
			LIST_NODE *prime = prime_numbers_dumb(k);
			LIST_NODE *new_head = nullptr;
			LIST_NODE *new_tail = nullptr;
			int fl = k + 1;
			int cl = 2 * k;
			while (fl <= n) {
				fl += (fl % 2 == 0);
				cl += (cl % 2 == 0);
				if (cl > n) cl = n;
				if (fl > cl) break;
				LIST_NODE *nums = new LIST_NODE(fl);
				fl += 2;
				LIST_NODE *tail = nums;
				while (fl <= cl) {
					tail->next = new LIST_NODE(fl);
					fl += 2;
					tail = tail->next;
				}
				fl = cl + 1;
				cl = fl + k;
				for (const LIST_NODE *ptr = prime; ptr; ptr = ptr->next) {
					LIST_NODE *prev = nullptr;
					LIST_NODE *curr = nums;
					while (curr) {
						LIST_NODE *next = curr->next;
						if (curr->val % ptr->val) prev = curr;
						else {
							if (next == nullptr) tail = prev;
							if (prev) prev->next = next;
							else nums = next;
							delete curr;
						}
						curr = next;
					}
				}
				if (nums) {
					new_head ? new_tail->next = nums : new_head = nums;
					new_tail = tail;
				}
			}
			LIST_NODE *tail = prime;
			while (tail->next) tail = tail->next;
			tail->next = new_head;
			return prime;
		}
		return prime_numbers_dumb(n);
	}
	void prime_numbers(int n, std::ostream &out) {
		if (n > 1) {
			if (n > 2000) {
				int k = sqrt(n);
				LIST_NODE *prime = prime_numbers_as_list(k);
				print_list(prime, out);
				std::cout << std::fixed << std::setprecision(6) << 100.0 * k / n << "% completed";
				int p = k / log2(k);
				int fl = k + 1;
				int cl = k + p;
				while (fl <= n) {
					fl += (fl % 2 == 0);
					cl += (cl % 2 == 0);
					if (cl > n) cl = n;
					if (fl > cl) break;
					LIST_NODE *nums = new LIST_NODE(fl);
					fl += 2;
					LIST_NODE *tail = nums;
					while (fl <= cl) {
						tail->next = new LIST_NODE(fl);
						fl += 2;
						tail = tail->next;
					}
					for (const LIST_NODE *ptr = prime; ptr; ptr = ptr->next) {
						LIST_NODE *prev = nullptr;
						LIST_NODE *curr = nums;
						while (curr) {
							LIST_NODE *next = curr->next;
							if (curr->val % ptr->val) prev = curr;
							else {
								if (next == nullptr) tail = prev;
								if (prev) prev->next = next;
								else nums = next;
								delete curr;
							}
							curr = next;
						}
					}
					print_list(nums, out);
					remove_list(nums);
					std::cout << '\r' << 100.0 * cl / n << "% completed";
					fl = cl + 1;
					cl = fl + p;
				}
				remove_list(prime);
				std::cout << std::endl;
			} else {
				LIST_NODE *ptr = prime_numbers_as_list(n);
				print_list(ptr, out);
				remove_list(ptr);
			}
		} else std::cout << "No prime numbers in range [1," << n << ']' << std::endl;
	}
};

int main(int argc, char **argv) {
	int n = 0;
	if (argc < 2) {
		std::cout << "Enter limit: ";
		std::cin >> n;
	} else {
		std::istringstream in(argv[1]);
		in >> n;
	}
	std::ofstream fout("output.txt");
	s284746::prime_numbers(n, fout);
	std::cout << "Result printed to output.txt" << std::endl;
	return 0;
}